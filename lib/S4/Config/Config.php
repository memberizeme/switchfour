<?php
namespace S4\Config;


class Config
	extends \S4\Core\Object
{
	private $settings;

	public function __construct() {
		$this->loadConfigFromFile("__default_path_to_config__");
	}

	public function __get($name) {
		if (array_key_exists($name, $this->settings)) {
			return $this->settings[$name];
		} else {
			return null;
		}
	}


	private function loadConfigFromFile($path) {
		$this->settings = "read content of file at specified $path";

	}
}