<?php
namespace S4\Http;

class Curl 
	extends \S4\Core\Object
{
	private $_url;
	private $_curl;


	public function setUrl($url) {
		$this->_url = $url;
	}

	public function getUrlContent() {
		$this->init_curl();
		$content = curl_exec($this->_curl);
		curl_close($this->_curl);

		// need properly handle curl errors 
		// throw exception and terminate execution

		if ($content !== false) {
			return $content;
		} else {
			echo "Unable to get content from url: {$this->_url}\n";
		}
	}

	private function init_curl() {
		if ($this->_url === null) {
			echo "URL is not specified";
		}

		unset($this->_curl);

		$this->_curl = curl_init();

		curl_setopt($this->_curl, CURLOPT_URL, $this->_url);
		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
	}
}