<?php

// Invoke common boostrap
require_once (__DIR__.'/../Bootstrap.php');


// Instantiate Shell Class
$shell = new \S4\Cli\Shell();

// Run shell command
$shell->exec($argv);