<?php
namespace S4\Cli\Command;

class Command 
	extends \S4\Core\Object
{
	private $config;
	protected $args;

	public function __construct($args) {
		$this->config = new \S4\Config\Config();

		$commandArgs = array_splice($args, 2);
		if (count($commandArgs) === 0) {
			$this->_args = array();
		} else {
			$this->processArguments($commandArgs);
		}
	}

	protected function processArguments($args) {
		foreach($args as $arg) {
			// if argument value contains equal sign this will break.
			// need more sophisticated mechanism
			$tmp = explode('=', $arg); 
			if (count($tmp) === 2) {
				$this->args[$tmp[0]] = $tmp[1];
			} else {
				$this->log("Invalid argument: {$arg}");
			}			
		}
	}

	protected function print($output) {
		print_r($output);
		echo ("\n");
	}
}