<?php
namespace S4\Cli\Command;

interface CommandInterface {
	public function exec();
}