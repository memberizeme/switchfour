<?php
namespace S4\Cli\Command;

use \S4\Http\Curl;
use \S4\Parse\Xml;

class Fetch 
	extends \S4\Cli\Command\Command
	implements \S4\Cli\Command\CommandInterface 
{
	public function exec() {
		if (array_key_exists('url', $this->args)) {
			$curl = new Curl();
			$curl->setUrl($this->args['url']);

			$content = $curl->getUrlContent();

			$xml = new Xml();
			$xml->load($content);

			if (array_key_exists('format', $this->args)) {
				switch($this->args['format']) {
					case 'json':
						$output = $xml->toJson();
						break;
					default:
						// validate format before actual $curl->getUrlContent() call
						// to avoid unnecessary execution which results in no output
						$this->log("Unsupported format: {$this->args['format']}");
						return;
				}
			} else {
				$output = $content;
			}

			$this->print($output);
		} else {
			$this->log('url is a required argument');
		}
	}
}