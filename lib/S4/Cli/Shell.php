<?php
namespace S4\Cli;

use \S4\Core\Object;
use \S4\Cli\Command\Fetch;

class Shell extends \S4\Core\Object
{
	public function __construct() {
		parent::__construct();
	}

	public function exec($argv) {
		if (count($argv) > 1) {
			switch(strtolower($argv[1])) {
				case "fetch":
					$subCommandName = "Fetch";
					break;
				default:
					$this->log("Unknown sub-command: {$argv[1]}");
					return;
			}

			$subCommandClass = "\\S4\\Cli\\Command\\".$subCommandName;

			$subcommand = new $subCommandClass($argv);
			$subcommand->exec();
		} else {
			$this->log("Please specify sub-command");
		}
	}
}