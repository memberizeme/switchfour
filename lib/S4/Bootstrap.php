<?php
require_once (__DIR__."/../Psr/Load/Psr4AutoloaderClass.php");
$loader = new \Psr\Load\Psr4AutoloaderClass();
$loader->register();
$loader->addNamespace('S4', __DIR__, true);