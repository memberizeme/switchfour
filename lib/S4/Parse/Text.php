<?php
namespace S4\Parse;

abstract class Text
	extends \S4\Core\Object
{
	protected $_content;

	public function toJson() {
		$tmp = json_encode($this->_content);
		if ($tmp === false) {
			$this->log("Unable to convert content to JSON");
		}
		return $tmp;
	}
}