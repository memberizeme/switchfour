<?php
namespace S4\Parse;


class Xml 
	extends \S4\Parse\Text
{
	public function load($content) {
		// need properly handle simplexml errors 
		// throw exception and terminate execution
		$this->_content = simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOCDATA);
	}
}