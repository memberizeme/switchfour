# README #
Test assignment for Switch Four PHP Developer position:
	1) write a class for CURL, the class should have a set URL method and a get method to return the results from the URL
	2) write a class that has a set XML method and get JSON method, this class will accept XML and translate it to JSON
	3) write a program that can be run from the command that utilizes the 2 previous classes
	4) the programs should utilize the CURL class to retrieve the data from the following URL
	https://travel.state.gov/_res/rss/TAs.xml
	5) Once the data is retrieved it should convert the XML to JSON using the second class and write it to standard out


### Requirements:
	PHP 5.3 and above
	SimpleXML
	CURL

### Usage:
	1. to run the program execute the s4 script located in /path/to/project/bin
	2. command format:
			s4 <sub-command> [url=<url to fetch>, format=<output format>] 
	3. subcommands:
			fetch - retreive the conent of url specified in corresponding argument
	4. arguments:
			url - a valid http url
			format - output format, currently supported values: [json]

### Usage examples
	1. bin/s4 fetch url=https://travel.state.gov/_res/rss/TAs.xml - execute from project root, will output raw server response
	2. /path/to/project/root/bin/s4 url=https://travel.state.gov/_res/rss/TAs.xml format=json - absolute path, with output format, will print json string